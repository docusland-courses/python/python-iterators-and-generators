import os

def readfiles(filenames : list):
    """ File opener generator for a given list of filenames. """
    pass

def print_long_lines(filename : str, length : int= 40) -> None:
    pass

def folder_to_listdir(folder):
    pass

def recurive_file_listing(filename_or_foldername):
    """ List all files of a giver folder name. """
    pass
    

def print_files(foldername:str) -> None:
    """ print filenames recursively. """
    pass

def nb_python_files(foldername:str) -> int:
    """ Parse python file recursively and count .py files. """
    pass

def nb_lines_in_python_files(foldername) -> int:
    """ Parse python files recursively and count coded lines. """
    pass

def split_file(nb_lines: int, file: str):
    """ split a file"""
    pass

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # print_long_lines('data/file.txt') # Affiche les deux premieres lignes du fichier
    # print_files('data')
    # assert nb_python_files('data') == 3
    # assert nb_lines_in_python_files('data') == 25
    # split_file(12, 'data/subfolder/main.py')
    pass