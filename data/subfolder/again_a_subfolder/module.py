""" Dumb module... Even though you can check out what memoize is """
def memoize(f):
    cache = {}
    def g(x):
        if x not in cache:
            cache[x] = f(x)
            return cache[x]
    return g